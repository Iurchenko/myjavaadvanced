package Test;

import java.util.Set;
import java.util.TreeSet;

public class CompareCollection {

    int a = 120;
    int b = 130;

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    // 1
    /**
     * Возвращает 0 если равны
     */
    public void compareByCompareTo() {
        Integer i = a;
        Integer j = b;

        System.out.println("compareByCompareTo - " + i.compareTo(b));
    }

    // 2
    /**
     * Возвращает true если равны
     */
    public void compareByEquals() {
        Integer i = a;
        Integer j = b;

        System.out.println("compareByEquals - " + i.equals(b));
    }

    // 3
    /**
     * Возвращает true если равны
     */
    public void compareByHashCode() {
        Integer i = a;
        Integer j = b;
        boolean status = false;

        if (i.hashCode() == j.hashCode())
            status = true;
        System.out.println("compareByHashCode - " + status);
    }

    // 4
    /**
     * Возвращает true если равны
     */
    public void compareByTernaryOperator() {
        Integer i = a;
        Integer j = b;

        boolean status = false;
        status = i.intValue() == j.intValue() ? true : false;

        System.out.println("compareByTernaryOperator - " + status);
    }

    // 5
    /**
     * Возвращает true если равны
     * 
     * @return
     */
    public void compareByACSIIcode(int a, int b) {

        boolean status = true;
        // Делаем String с наших переменных
        String tmpA = String.valueOf(a);
        String tmpB = String.valueOf(b);

        // Если длинна строк равна, проверяем посимвольно по таблице ASCII. Если не
        // равна - результат false
        if (tmpA.length() == tmpB.length()) {
            for (int i = 0; i < tmpA.length(); i++) {
                if (status) {
                    if (tmpA.charAt(i) == tmpB.charAt(i)) {
                    } else {
                        status = false;
                    }
                }
            }
        } else
            status = false;

        System.out.println("compareByACSIIcode - " + status);
    }

    // 6
    /**
     * Возвращает true если равны. Использует contentEquals, который проверяет
     * содержимое строки(контент)
     * 
     */
    public void compareByString() {

        String s1 = Integer.valueOf(a).toString();
        String s2 = Integer.valueOf(b).toString();
        boolean status = false;

        if (s1.contentEquals(s2))
            status = true;

        System.out.println("compareByString - " + status);
    }

    // 7
    /**
     * Возвращает 0 если равны
     */
    public void compareByIntegerCompareMethod() {

        int rezult = Integer.compare(a, b);

        System.out.println("compareByIntegerCompareMethod - " + rezult);

    }

    // 8
    /**
     * При равных переменных в рознице min и max получим 0
     */
    public void compareByIntegerMINMAX() {

        int rezult = Integer.max(a, b) - Integer.min(a, b);
        boolean status = false;

        if (rezult == 0)
            status = true;
        System.out.println("compareByIntegerMINMAX -" + status);

    }

    // 9
    /**
     * Используем assertTrue или assumeTrue. Равны если тест проходит
     */
    public void compareByTestAssertORAssume() {
        // assertTrue((a-b)>0);
        System.out.println("compareByTestAssertORAssume - можно проверить запустив соответстыующий тест");

    }

    // 10
    /**
     * Схож с IntegerMIN&MAX, но с другой библиотеки.
     */
    public void compareByMathMinMax() {

        System.out.println("compareByMathMinMax - " + ((Math.max(a, b) - Math.min(a, b)) == 0 ? true : false));
    }

    // 11
    /**
     * С помощью toBinaryString() получаем бинарное представление. Сравниваем
     * побитно Если равны - получим 0;
     */
    public void compareByBynaryString() {
        String s1 = Integer.toBinaryString(a);
        String s2 = Integer.toBinaryString(b);
        boolean status = false;
        int count = 0;

        if (s1.length() > s2.length()) {
            status = true;
        }

        if (s1.length() == s2.length()) {
            for (int i = 0; i < s1.length(); i++) {
                if (s1.charAt(i) > s2.charAt(i)) {
                    status = true;
                    System.out.println("compareByBynaryString - " + status);
                    return;
                }
                if (s1.charAt(i) < s2.charAt(i)) {
                    status = false;
                    System.out.println("compareByBynaryString - " + status);
                    return;
                }
                if (s1.charAt(i) == s2.charAt(i)) {
                    count++;
                }
            }
        } else if (s1.length() < s2.length())
            status = false;

        if (count == s1.length()) {
            System.out.println("compareByBynaryString - " + 0);
            return;
        }
        System.out.println("compareByBynaryString - " + status);
    }

    // 12
    /**
     * Используем subtractExact(a, b); Он возвращает разницу между едементами Если
     * равны - получим 0;
     */
    public void compareByMathSubtractExact() {
        int rezult = Math.subtractExact(a, b);
        if (rezult == 0) {
            System.out.println("compareByMathsubtractExact - " + 0);
            return;
        }

        System.out.println("compareByMathsubtractExact - " + (rezult > 0 ? true : false));
    }

    // 13
    /**
     * Добавляем 2 елемента в коллекцию, которая не может иметь дубликатов Если
     * после добавления ее размер равен 1 - числа равны
     */
    public void compareByTreeSet() {
        Set<Integer> set = new TreeSet<Integer>();
        set.add(a);
        set.add(b);
        boolean status = false;

        int len = set.size();
        if (len == 1)
            status = true;
        System.out.println("compareByTreeSet - " + status);
    }

    // 14
    /**
     * Возвращает true если равны. Использует endsWith, который проверяет суффикс В
     * качестве суффикса подставляем строку s2 полностью и проверяем на length
     */
    public void compareByStringSuffix() {

        String s1 = Integer.valueOf(a).toString();
        String s2 = Integer.valueOf(b).toString();
        boolean status = false;

        if (s1.endsWith(s2) && s1.length() == s2.length())
            status = true;

        System.out.println("compareByStringSuffix - " + status);
    }

    // 15
    /**
     * Возвращает true если равны. Использует contains, который проверяет вхождение
     * строки Сделав проверку на length получаем равенство при вхождении строки
     */
    public void compareByStringContains() {

        String s1 = Integer.valueOf(a).toString();
        String s2 = Integer.valueOf(b).toString();
        boolean status = false;

        if (s1.contains(s2) && s1.length() == s2.length())
            status = true;

        System.out.println("compareByStringContains - " + status);
    }

}
