package Test;

public class Main {
    public static void main(String[] args) {
        int a = 100;
        int b = 100;
        CompareCollection c = new CompareCollection();

        c.compareByCompareTo();
        c.compareByEquals();
        c.compareByHashCode();
        c.compareByTernaryOperator();
        c.compareByACSIIcode(a,b); //a and b for this method
        c.compareByString();
        c.compareByIntegerCompareMethod();
        c.compareByIntegerMINMAX();
        c.compareByTestAssertORAssume(); 
        c.compareByMathMinMax();
        c.compareByBynaryString();
        c.compareByMathSubtractExact();
        c.compareByTreeSet();
        c.compareByStringSuffix();
        c.compareByStringContains();
    }
}
