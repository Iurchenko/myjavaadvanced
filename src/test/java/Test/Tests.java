package Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import org.junit.Test;

public class Tests {
    @Test
    public void compareByTestAssert() {
        CompareCollection cc = new CompareCollection();

        assertTrue((cc.getA()-cc.getB())==0);
        assumeTrue((cc.getA()-cc.getB())==0);
    }

}
